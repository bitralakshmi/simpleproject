package com.example.demo;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Email {

	public static void main(String[] args) {

Scanner input = new Scanner(System.in);
		
		
		String s= input.nextLine();
		 
	        if (isValid(s)) 
	            System.out.print("Yes"); 
	        else
	            System.out.print("No");
	}

	private static boolean isValid(String s) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                "[a-zA-Z0-9_+&*-]+)*@" + 
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex); 
        if (s == null) 
            return false; 
        return pat.matcher(s).matches(); 
	}

	

}
